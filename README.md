# Vue 3 Data Visualization

# Vue 3 数据可视化

## 项目简介

本项目为个人项目, 主要使用 `Vue 3`, `Echarts 5`, `Element Plus` 进行开发. 使用原生 `JavaScript` 实现元素的拖拽与缩放.

该项目主要为实现通用的可视化大屏的简单配置, 对于特殊图表仍然需要二次开发.

项目预览: [https://miyuesc.github.io/data-visualization/](https://miyuesc.github.io/data-visualization/)

国内同步镜像: [https://miyuesc.gitee.io/data-visualization/](https://miyuesc.gitee.io/data-visualization/)

> 预览地址可能会存在版本不符的问题, 建议下载运行

## 主要功能

已实现 缩放/拖拽, 标题单位, 背景边框, 坐标轴, 层级移动, 画布缩放 等.

## 使用

### 1. 下载或克隆该项目到本地

```shell
git clone https://github.com/miyuesc/vue-data-visualization.git -b main
```

### 2. 安装依赖

```shell
// yarn run install
npm run install
```

### 3. 运行预览

```shell
// yarn run build
npm run build
```



## 运行预览

### 1. 初始界面

![image-20210418152927141](README/static/image-20210418152927141.png)

### 2. 拖拽效果

![image-20210418153707284](README/static/image-20210418153707284.png)

### 3. 标题与单位配置

![image-20210418153739172](README/static/image-20210418153739172.png)

### 4. 背景边框配置

![image-20210418153932966](README/static/image-20210418153932966.png)

### 5. X轴/Y轴配置

![image-20210418154052623](README/static/image-20210418154052623.png)
